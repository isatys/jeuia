Nous avons deux projets fonctionnels. L'API rest qui va permettre de voir les pseudos enregistrés sur le jeu.
On peut aussi en ajouter depuis le web ou bien même modifier les pseudos déjà existant.
Le "read me" de ce projet se trouve donc dans le "isatysDjangoRest" pour comprendre comment afficher la page web.

On aura ensuite le projet du jeu, se trouvant dans "LabyrintheIA".
Qui permettra de lancer le jeu. 
Le "read me" est aussi disponible dans ce projet pour que l'on puisse savoir comment lancer le jeu.
