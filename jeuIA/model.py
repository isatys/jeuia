from sqlalchemy import Column, String, Integer, Boolean, ForeignKey, DateTime
from sqlalchemy.orm import declarative_base

Base = declarative_base()


class User(Base):
    __tablename__ = 'user'
    id = Column(Integer, primary_key=True)
    name = Column('name', String(255))
    active = Column('active', Boolean)
    firstname = Column('firstname', String(255))
    lastLogin = Column('lastLogin', DateTime)


class Level(Base):
    __tablename__ = 'level'
    id = Column(Integer, primary_key=True)
    name = Column('name', String(255))
    num = Column('num', Integer)


class UserLevel(Base):
    __tablename__ = 'user_level'
    id = Column(Integer, primary_key=True)
    user_id = Column('user_id', ForeignKey('user.id'), nullable=False)
    level_id = Column('level_id', ForeignKey('level.id'), nullable=False)


class Score(Base):
    __tablename__ = 'score'
    id = Column(Integer, primary_key=True)
    score = Column('score', Integer)
