"""
Ce jeu aura pour première page , 2 bouttons qui permettra de choisir le niveau à prendre.
Le premier niveau sera pour les utilisateurs voulant un labyrinthe déjà crée.
Le deuxième niveau servira à éditer un labyrinthe
"""
from tkinter import messagebox, Tk
from typing import List

import math
import pygame
import sys
import logging

from pygame_menu.examples import create_example_window

logging.basicConfig()

# create a logger
logger = logging.getLogger('mylogger')
# set logging level
logger.setLevel(logging.DEBUG)

handler = logging.FileHandler(
    '../../../../../AppData/Local/Packages/microsoft.windowscommunicationsapps_8wekyb3d8bbwe/LocalState/Files/S0/3041/Attachments/isatys[12699]/isatys/pages/labyrinthe/mylog.log')
# create a logging format
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)

screen = (width, height) = 600, 450
ecran = pygame.display.set_mode(screen)
clock = pygame.time.Clock()
cols, rows = 10, 10
grid = []
openSet, closeSet = [], []
path = []

w = width // cols
h = height // rows


class CreationMaze(object):
    """
    :ivar x:
    :ivar cordY:
    :ivar f:
    :ivar g:
    :ivar h:
    :ivar casesVoisines:
    :ivar prev:
    :ivar wall:
    """

    def __init__(self, iCord, jCord):
        """

        :param iCord:
        :param jCord:
        """
        self.x, self.cordY = iCord, jCord
        self.f, self.g, self.h = 0, 0, 0
        self.casesVoisines = []
        self.prev = None
        self.wall = False

    def dessin(self, surface, col):
        """

        :param surface:
        :param col:
        :return: myself
        """
        if self.wall:
            col = (0, 0, 0)
        pygame.draw.rect(surface, col, (self.x * w, self.cordY * h, w - 1, h - 1))
        return self

    def ajout_casesVoisines(self, gridTable):
        """

        :param gridTable:
        :return: myself
        """
        if self.x < cols - 1:
            self.casesVoisines.append(gridTable[self.x + 1][self.cordY])
        if self.x > 0:
            self.casesVoisines.append(gridTable[self.x - 1][self.cordY])
        if self.cordY < rows - 1:
            self.casesVoisines.append(gridTable[self.x][self.cordY + 1])
        if self.cordY > 0:
            self.casesVoisines.append(gridTable[self.x][self.cordY - 1])
        # Ajout de diagonal
        if self.x < cols - 1 and self.cordY < rows - 1:
            self.casesVoisines.append(gridTable[self.x + 1][self.cordY + 1])
        if self.x < cols - 1 and self.cordY > 0:
            self.casesVoisines.append(gridTable[self.x + 1][self.cordY - 1])
        if self.x > 0 and self.cordY < rows - 1:
            self.casesVoisines.append(gridTable[self.x - 1][self.cordY + 1])
        if self.x > 0 and self.cordY > 0:
            self.casesVoisines.append(gridTable[self.x - 1][self.cordY - 1])
        return self


def Wall(pos, state):
    """

    :param pos:
    :param state:
    :return: int
    """
    iCord = pos[0] // w
    jCord = pos[1] // h
    grid[iCord][jCord].wall = state
    return state


def choix(a, b):
    """

    :param a:
    :param b:
    :return:int
    """
    return math.sqrt((a.x - b.x) ** 2 + abs(a.cordY - b.cordY) ** 2)  # Chemin tracé part l'utilisateur


for i in range(cols):
    arr = []
    for j in range(rows):
        arr.append(CreationMaze(i, j))
    grid.append(arr)

for i in range(cols):
    for j in range(rows):
        grid[i][j].ajout_casesVoisines(grid)

start = grid[0][0]
end = grid[cols - cols // 2][rows - cols // 4]

openSet.append(start)


def close():
    """

    :return:
    """
    pygame.quit()
    sys.exit()


class Game(object):
    """
    :ivar WINDOW_SIZE: size for the screen window
    """

    def __init__(self):
        """

        """

        self.WINDOW_SIZE = (600, 450)

    def main(self, value: List):
        """

        :param value:
        :return: int
        """
        flag = False
        noflag = True
        startflag = False

        create_example_window('Python', self.WINDOW_SIZE)

        assert isinstance(value, list)
        value = value[0]
        assert isinstance(value, int)
        while True:

            # noinspection PyUnresolvedReferences
            clock.tick(60)

            # Application events
            events = pygame.event.get()
            for event in events:
                if event.type == pygame.QUIT:
                    exit()
                if event.type == pygame.MOUSEBUTTONUP:
                    if pygame.mouse.get_pressed(3):
                        Wall(pygame.mouse.get_pos(), True)
                    if pygame.mouse.get_pressed(3):
                        Wall(pygame.mouse.get_pos(), False)
                if event.type == pygame.MOUSEMOTION:
                    if pygame.mouse.get_pressed(3)[0]:
                        Wall(pygame.mouse.get_pos(), True)
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_RETURN:
                        startflag = True

            if startflag:
                if len(openSet) > 0:
                    winner = 0
                    for iCord in range(len(openSet)):
                        if openSet[iCord].f < openSet[winner].f:
                            winner = iCord

                    current = openSet[winner]

                    if current == end:
                        temp = current
                        while temp.prev:
                            path.append(temp.prev)
                            temp = temp.prev
                        if not flag:
                            flag = True
                            print("Résultat")
                        elif flag:
                            continue

                    if not flag:
                        openSet.remove(current)
                        closeSet.append(current)

                        for neighbor in current.casesVoisines:
                            if neighbor in closeSet or neighbor.wall:
                                continue
                            tempG = current.g + 1

                            newPath = False
                            if neighbor in openSet:
                                if tempG < neighbor.g:
                                    neighbor.g = tempG
                                    newPath = True
                            else:
                                neighbor.g = tempG
                                newPath = True
                                openSet.append(neighbor)

                            if newPath:
                                neighbor.h = choix(neighbor, end)
                                neighbor.f = neighbor.g + neighbor.h
                                neighbor.prev = current

                else:
                    if noflag:
                        Tk().wm_withdraw()
                        messagebox.showinfo("No Solution", "There was no solution")
                        noflag = False

            ecran.fill((0, 20, 20))  # Couleur du fond et des murs
            for iCord in range(cols):
                for jCord in range(rows):
                    locate = grid[jCord][iCord]
                    locate.dessin(ecran, (96, 125, 139))  # Couleur carrés du fond
                    if flag and locate in path:
                        locate.dessin(ecran, (41, 182, 246))  # Départ
                    elif locate in closeSet:
                        locate.dessin(ecran, (128, 222, 234))  # Le chemin le plus court
                    elif locate in openSet:
                        locate.dessin(ecran, (0, 96, 100))  # Les chemins voisins
                    try:
                        if locate == end:
                            locate.dessin(ecran, (0, 120, 255))  # Arrivée
                    except Exception:
                        logger.debug("Warning")

            pygame.display.flip()
        return value
