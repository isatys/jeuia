import logging
from datetime import datetime

from requests import request

import conf
from Helpers.mysql import MysqlHelper

logger = logging.getLogger(__name__)


class UserGame(object):
    """
    User management class
    """
    def __init__(self):
        """
        Init the class
        """
        self.conf = conf

    def save_user(self, name=None):
        """
        Save user name router
        """
        if conf.PREFERED_STORAGE_METHOD == "sqlalchemy":
            self.save_user_sqlalchemy(name=name)
        elif conf.PREFERED_STORAGE_METHOD == "rest":
            self.save_user_rest(name=name)

    def save_user_rest(self, name=None):
        """
        save user using rest method
        :param name: The username to save
        :type name: str
        :return: void
        """
        # seek user
        url = "{}/mazeuser/".format(self.conf.REST_URL)
        response = request('GET', url=url)
        if response.status_code == 202:
            users = response.json()
            found = False
            for user in users:
                # if found update
                if user['username'] == name:
                    found = True
                    data = {
                        "username": name,
                        "lastLogin": datetime.now()
                    }
                    response_set = request('PUT', url=url, data=data)
                    if response_set.status_code != 201:
                        logger.error("Cant update user: %s", response_set.text)
                        raise Exception('Cant put to rest server')

            # if not found create
            # separate if we want to add data at creation
            if not found:
                data = {
                    "username": name,
                    "lastLogin": datetime.now()
                }
                response_set = request('POST', url=url, data=data)
                if response_set.status_code != 201:
                    logger.error("cant create user: %", response_set.text)
                    raise Exception("Cant create user")
        else:
            raise Exception('Cant get user from rest server')

    def save_user_sqlalchemy(self, name=None):
        """
        Save user name using sqlalchemy
        :param name: The username to save
        :type name: str
        :return: void
        """
        conn = MysqlHelper(login=self.conf.LOGIN, password=self.conf.PASSWORD, mysql_db=self.conf.DB)
        conn.init()
        # seek user exists
        users = conn.get_user(name)
        # if not save
        if len(users) > 0:
            conn.uodate_login(users[0]['id'])
        # if found update last login date
        else:
            conn.add_user(username=name, firstname=name)
