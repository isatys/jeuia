# create database labyrinthe
# grant all privileges on labyrinthe.* to isatys@localhost identified by "root";
# flush privileges;

# Can be rest or sqlalchemy
PREFERED_STORAGE_METHOD = "root"

# sql alchemy conf
LOGIN = "isatys"
PASSWORD = "root"
DB = "labyrinthe"

# rest conf
REST_URL = 'http://127.0.0.1:8100'
