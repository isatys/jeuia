from datetime import datetime

from sqlalchemy import create_engine
from sqlalchemy.orm import registry, scoped_session, sessionmaker

from model import Base, User, Level


class MysqlHelper(object):
    """
    MySQL Helpers class
    """

    def __init__(self, login=None, password=None, mysql_host="localhost", mysql_db=None, mysql_port=3306):
        """
        Init the class
        :type login: str
        :param login: The MySQL password
        :type password: str
        :param password: The MySQL login
        :type mysql_host: str
        :param mysql_host: The MySQL host ip or name
        :type mysql_db: str
        :param mysql_db: The MySQL database name
        :type mysql_port: int
        :param mysql_port: The MySQL TCP port (default to 3306)

        """
        self.login = login
        self.password = password
        self.mysql_host = mysql_host
        self.mysql_db = mysql_db
        self.mysql_port = mysql_port
        self.engine = None
        self.session = None
        self.registry = None
        self.base = None

    def init(self):
        """
        Init a connection and create database
        """
        self.connect()
        self.create_session()
        self.create_registry()
        self.create_schema()

    def connect(self):
        """
        Connect to the db

        :return: engineclass

        """

        self.engine = create_engine('mysql+pymysql://{login}:{password}@{host}:{port}/{db}'.format(
            login=self.login,
            password=self.password,
            host=self.mysql_host,
            port=self.mysql_port,
            db=self.mysql_db
        ), echo=True)
        return self.engine

    def create_session(self):
        """
        Create a session on db

        :return: Session
        """
        self.session = scoped_session(
            sessionmaker(
                bind=self.engine,
                autocommit=False,
                autoflush=False
            )
        )

    def create_schema(self):
        """
        Create db schema
        """
        Base.metadata.create_all(bind=self.engine)

    def create_registry(self):
        """
        Create a registry
        """
        self.registry = registry()

    def add_user(self, username=None, firstname=None, active=True):
        """
        Add an user

        :param username: The username
        :type username: str
        :param firstname: The firstname
        :type firstname: str
        :param active: Is user active
        :return: bool
        """
        if not username or not firstname:
            return False
        self.session.add(
            User(name=username, active=active, firstname=firstname, lastLogin=datetime.now())
        )
        self.session.commit()
        return True

    def add_level(self, name=None, num=None):
        """
        Add a new level

        :param name: The level name
        :type name: str
        :param num: The level num
        :type num: int
        :return: bool
        """
        if not name or not num:
            return False
        self.session.add(
            Level(name=name, num=num)
        )
        self.session.commit()
        return True

    def get_user(self, name=None):
        """
        Search a user per username

        :param name: The user name
        :type name: str

        :return: list
        """
        if not name:
            raise Exception("No name provided")
        lst = []
        for user in self.session.query(User).filter(User.name == name):
            lst.append({"id": user.id, "name": user.name, "firstname": user.firstname, "active": user.active})
        return lst

    def update_user(self, name=None, new_name=None):
        """
        Search a user per username

        :param name: The user name
        :type name: str
        :param new_name: The new user name
        :type new_name: str

        :return: dict
        """
        if not name or not new_name:
            raise Exception("No name provided")
        for user in self.session.query(User).filter(User.name == name):
            user.name = new_name
        res = self.session.commit()
        return res

    def uodate_login(self, userId=None):
        """
        Update login date for a user
        :param userId: the user id
        :type userId: int

        :return: bool
        """
        if not userId:
            raise Exception('No id provided')

        for user in self.session.query(User).filter(User.id == userId):
            user.lastLogin = datetime.now()
        res = self.session.commit()
        return res
