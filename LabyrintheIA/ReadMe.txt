Vous retrouverez dans ce projet un mini jeu.
Ce jeu s'intitule "LabyrinteIA".

Vous devrez lancer le fichier menu.py pour accéder au début du jeu.

Il vous faudra entrer un nom de jeu puis choisir votre niveau( cela n'est pas encore fonctionnel, vous pouvez juste
appercevoir dans le terminal si vous avez choisi le niveau 1 ou deux).

Ensuite appuiez sur le bouton "play".

Le jeu commence, il vous faudra donc commencez par placé vous-même les murs qui seront dessinés en noir.

Si vous appuyez de nouveau sur le mur dessiné vous pourrez l'enlever.

Quand votre labyrinthe vous semble terminer appuyez sur ENTRER pour lancer l'intelligence artificielle.