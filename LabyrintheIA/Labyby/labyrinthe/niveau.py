from typing import Tuple, List

from pygame_menu.examples import create_example_window


class Level(object):
    """

    """

    def __init__(self):
        """

        """
        self.LEVEL = [1]

    @staticmethod
    def level(value: List):
        """
        ------------------------Niveau pas fait--------------------------------------------------------
        :return:
        """

        WINDOW_SIZE = (600, 450)
        create_example_window('Python', WINDOW_SIZE)
        assert isinstance(value, list)
        value = value[0]
        assert isinstance(value, int)
        if value == 1:
            pass

        if value == 2:
            pass

    def set_level(self, selected: Tuple, value: int):
        """

        :param selected:
        :param value:
        :return:
        """
        print('Set level to {} ({})'.format(selected[0], value))
        self.LEVEL[0] = value
