from typing import List

import pygame
import pygame_menu
from pygame_menu.examples import create_example_window
from labyrinthe import Game
from niveau import Level
from Labyby.labyrinthe.usergame import UserGame

lock = pygame.time.Clock()


class Menu(object):
    """

    """

    def __init__(self):
        """

        """
        WINDOW_SIZE = (600, 450)
        self.surface = create_example_window('Example - Game Selector', WINDOW_SIZE)
        self.FPS = 60
        self.menu = pygame_menu.Menu('Welcome', 300, 400,
                                     theme=pygame_menu.themes.THEME_DARK)
        self.nameInput = None

    def main_background(self):
        """

        :return: None
        """
        self.surface.fill((109, 230, 195))

    def launch_game(self, value: List):
        """
        Save name and launch game

        :param value: The list of level ?
        :type value: list
        """
        UserGame().save_user(self.nameInput.get_value())
        Game().main(value)

    def menu_game(self):
        """

        :return:
        """
        self.nameInput = self.menu.add.text_input('Name :', default='John Doe', onreturn=UserGame().save_user)
        self.menu.add.selector('Level: ', [('Level 1', 1), ('Level 2', 2)], onchange=Level().set_level)
        self.menu.add.button('Play', self.launch_game, Level().LEVEL)
        self.menu.add.button('Quit', pygame_menu.events.EXIT)

        while True:

            # Tick
            lock.tick(self.FPS)

            # Paint background
            self.main_background()

            # Application events
            events = pygame.event.get()
            for event in events:
                if event.type == pygame.QUIT:
                    exit()

            # Main gameMenu
            if self.menu.is_enabled():
                self.menu.mainloop(self.surface, self.main_background, fps_limit=self.FPS)

            # Flip surface
            pygame.display.flip()


# Appel de ma fonction gameMenu
gameMenu = Menu()
gameMenu.menu_game()
