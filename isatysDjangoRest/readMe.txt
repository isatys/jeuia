Pour lancer l'API rest vous devez d'abord avoir créé une base de données MySQL qui sera mise en relation avec ce projet.
Elle devra s'appeler "labyrinthe".

Il faudra taper la commande : "python manage.py migrate".

Pour permettre de changer le level, il vous faudra obligatoirement un login.

Donc utilisé la commande suivante pour vous créer un compte : "python manage.py createsuperuser".

Puis lancez la commande "python manage.py runserver 8100", le port est celui que vous avez choisi pour votre jeu.