from rest_framework import viewsets, status
from rest_framework import permissions
from rest_framework.response import Response

from isatysDjangoRest.models import MazeUser, MazeLevel
from isatysDjangoRest.serializers import MazeUserSerializer, MazeLevelSerializer


class MazeUserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows maze users to be viewed or edited.
    """
    queryset = MazeUser.objects.all().order_by('-lastLogin')
    serializer_class = MazeUserSerializer
    permission_classes = [permissions.AllowAny]

    @staticmethod
    def put(request):
        instance = MazeUser.objects.get(username=request.POST['username'])
        serializer = MazeUserSerializer(instance, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(status=status.HTTP_202_ACCEPTED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class MazeLevelViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = MazeLevel.objects.all()
    serializer_class = MazeLevelSerializer
    permission_classes = [permissions.IsAuthenticated]
