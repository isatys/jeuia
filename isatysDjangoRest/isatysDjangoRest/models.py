import django
from django.db import models
from django.utils.timezone import now


class MazeUser(models.Model):
    """
    MazeUser object
    """
    username = models.CharField(max_length=255, null=False, blank=False, unique=True)
    lastLogin = models.DateTimeField(default=django.utils.timezone.now, blank=False, null=False)
    active = models.BooleanField(default=True)

    class Meta:
        """
        Meta class to set index
        """
        indexes = [
            models.Index(fields=['username']),
        ]


class MazeLevel(models.Model):
    """
    MazeLevel object
    """
    name = models.CharField(max_length=255, null=False, blank=False)
    num = models.IntegerField(default=1)
