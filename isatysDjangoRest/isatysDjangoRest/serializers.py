from datetime import datetime

from rest_framework import serializers

from isatysDjangoRest.models import MazeUser, MazeLevel


class MazeUserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = MazeUser
        fields = ['username', 'lastLogin']

    def update(self, instance, validated_data):
        """
        update method to update lastLogin
        :return: instance
        """
        instance.username = validated_data.get('username', instance.username)
        if 'lastLogin' not in validated_data:
            validated_data['lastLogin'] = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            instance.lastLogin = validated_data['lastLogin']
        instance.save()
        return instance


class MazeLevelSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = MazeLevel
        fields = ['name', 'num']
